<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Test Report</title>
    <link href="assets/style.css" rel="stylesheet" type="text/css"/></head>
  <body onLoad="init()">
    <script>/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */


function toArray(iter) {
    if (iter === null) {
        return null;
    }
    return Array.prototype.slice.call(iter);
}

function find(selector, elem) { // eslint-disable-line no-redeclare
    if (!elem) {
        elem = document;
    }
    return elem.querySelector(selector);
}

function findAll(selector, elem) {
    if (!elem) {
        elem = document;
    }
    return toArray(elem.querySelectorAll(selector));
}

function sortColumn(elem) {
    toggleSortStates(elem);
    const colIndex = toArray(elem.parentNode.childNodes).indexOf(elem);
    let key;
    if (elem.classList.contains('result')) {
        key = keyResult;
    } else if (elem.classList.contains('links')) {
        key = keyLink;
    } else {
        key = keyAlpha;
    }
    sortTable(elem, key(colIndex));
}

function showAllExtras() { // eslint-disable-line no-unused-vars
    findAll('.col-result').forEach(showExtras);
}

function hideAllExtras() { // eslint-disable-line no-unused-vars
    findAll('.col-result').forEach(hideExtras);
}

function showExtras(colresultElem) {
    const extras = colresultElem.parentNode.nextElementSibling;
    const expandcollapse = colresultElem.firstElementChild;
    extras.classList.remove('collapsed');
    expandcollapse.classList.remove('expander');
    expandcollapse.classList.add('collapser');
}

function hideExtras(colresultElem) {
    const extras = colresultElem.parentNode.nextElementSibling;
    const expandcollapse = colresultElem.firstElementChild;
    extras.classList.add('collapsed');
    expandcollapse.classList.remove('collapser');
    expandcollapse.classList.add('expander');
}

function showFilters() {
    const filterItems = document.getElementsByClassName('filter');
    for (let i = 0; i < filterItems.length; i++)
        filterItems[i].hidden = false;
}

function addCollapse() {
    // Add links for show/hide all
    const resulttable = find('table#results-table');
    const showhideall = document.createElement('p');
    showhideall.innerHTML = '<a href="javascript:showAllExtras()">Show all details</a> / ' +
                            '<a href="javascript:hideAllExtras()">Hide all details</a>';
    resulttable.parentElement.insertBefore(showhideall, resulttable);

    // Add show/hide link to each result
    findAll('.col-result').forEach(function(elem) {
        const collapsed = getQueryParameter('collapsed') || 'Passed';
        const extras = elem.parentNode.nextElementSibling;
        const expandcollapse = document.createElement('span');
        if (extras.classList.contains('collapsed')) {
            expandcollapse.classList.add('expander');
        } else if (collapsed.includes(elem.innerHTML)) {
            extras.classList.add('collapsed');
            expandcollapse.classList.add('expander');
        } else {
            expandcollapse.classList.add('collapser');
        }
        elem.appendChild(expandcollapse);

        elem.addEventListener('click', function(event) {
            if (event.currentTarget.parentNode.nextElementSibling.classList.contains('collapsed')) {
                showExtras(event.currentTarget);
            } else {
                hideExtras(event.currentTarget);
            }
        });
    });
}

function getQueryParameter(name) {
    const match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function init () { // eslint-disable-line no-unused-vars
    resetSortHeaders();

    addCollapse();

    showFilters();

    sortColumn(find('.initial-sort'));

    findAll('.sortable').forEach(function(elem) {
        elem.addEventListener('click',
            function() {
                sortColumn(elem);
            }, false);
    });
}

function sortTable(clicked, keyFunc) {
    const rows = findAll('.results-table-row');
    const reversed = !clicked.classList.contains('asc');
    const sortedRows = sort(rows, keyFunc, reversed);
    /* Whole table is removed here because browsers acts much slower
     * when appending existing elements.
     */
    const thead = document.getElementById('results-table-head');
    document.getElementById('results-table').remove();
    const parent = document.createElement('table');
    parent.id = 'results-table';
    parent.appendChild(thead);
    sortedRows.forEach(function(elem) {
        parent.appendChild(elem);
    });
    document.getElementsByTagName('BODY')[0].appendChild(parent);
}

function sort(items, keyFunc, reversed) {
    const sortArray = items.map(function(item, i) {
        return [keyFunc(item), i];
    });

    sortArray.sort(function(a, b) {
        const keyA = a[0];
        const keyB = b[0];

        if (keyA == keyB) return 0;

        if (reversed) {
            return keyA < keyB ? 1 : -1;
        } else {
            return keyA > keyB ? 1 : -1;
        }
    });

    return sortArray.map(function(item) {
        const index = item[1];
        return items[index];
    });
}

function keyAlpha(colIndex) {
    return function(elem) {
        return elem.childNodes[1].childNodes[colIndex].firstChild.data.toLowerCase();
    };
}

function keyLink(colIndex) {
    return function(elem) {
        const dataCell = elem.childNodes[1].childNodes[colIndex].firstChild;
        return dataCell == null ? '' : dataCell.innerText.toLowerCase();
    };
}

function keyResult(colIndex) {
    return function(elem) {
        const strings = ['Error', 'Failed', 'Rerun', 'XFailed', 'XPassed',
            'Skipped', 'Passed'];
        return strings.indexOf(elem.childNodes[1].childNodes[colIndex].firstChild.data);
    };
}

function resetSortHeaders() {
    findAll('.sort-icon').forEach(function(elem) {
        elem.parentNode.removeChild(elem);
    });
    findAll('.sortable').forEach(function(elem) {
        const icon = document.createElement('div');
        icon.className = 'sort-icon';
        icon.textContent = 'vvv';
        elem.insertBefore(icon, elem.firstChild);
        elem.classList.remove('desc', 'active');
        elem.classList.add('asc', 'inactive');
    });
}

function toggleSortStates(elem) {
    //if active, toggle between asc and desc
    if (elem.classList.contains('active')) {
        elem.classList.toggle('asc');
        elem.classList.toggle('desc');
    }

    //if inactive, reset all other functions and add ascending active
    if (elem.classList.contains('inactive')) {
        resetSortHeaders();
        elem.classList.remove('inactive');
        elem.classList.add('active');
    }
}

function isAllRowsHidden(value) {
    return value.hidden == false;
}

function filterTable(elem) { // eslint-disable-line no-unused-vars
    const outcomeAtt = 'data-test-result';
    const outcome = elem.getAttribute(outcomeAtt);
    const classOutcome = outcome + ' results-table-row';
    const outcomeRows = document.getElementsByClassName(classOutcome);

    for(let i = 0; i < outcomeRows.length; i++){
        outcomeRows[i].hidden = !elem.checked;
    }

    const rows = findAll('.results-table-row').filter(isAllRowsHidden);
    const allRowsHidden = rows.length == 0 ? true : false;
    const notFoundMessage = document.getElementById('not-found-message');
    notFoundMessage.hidden = !allRowsHidden;
}
</script>
    <h1>report.md</h1>
    <p>Report generated on 03-Sep-2021 at 14:20:42 by <a href="https://pypi.python.org/pypi/pytest-html">pytest-html</a> v3.1.1</p>
    <h2>Environment</h2>
    <table id="environment">
      <tr>
        <td>Packages</td>
        <td>{"pluggy": "0.13.1", "py": "1.10.0", "pytest": "6.2.4"}</td></tr>
      <tr>
        <td>Platform</td>
        <td>Linux-5.13.13-arch1-1-x86_64-with-glibc2.33</td></tr>
      <tr>
        <td>Plugins</td>
        <td>{"bdd": "4.0.2", "benchmark": "3.4.1", "cov": "2.12.1", "forked": "1.3.0", "html": "3.1.1", "hypothesis": "6.13.14", "icdiff": "0.5", "instafail": "0.4.2", "md": "0.2.0", "metadata": "1.11.0", "mock": "3.6.1", "qt": "4.0.0", "repeat": "0.9.1", "rerunfailures": "10.0", "xdist": "2.2.1", "xvfb": "2.0.0"}</td></tr>
      <tr>
        <td>Python</td>
        <td>3.9.6</td></tr></table>
    <h2>Summary</h2>
    <p>129 tests ran in 4.15 seconds. </p>
    <p class="filter" hidden="true">(Un)check the boxes to filter the results.</p><input checked="true" class="filter" data-test-result="passed" hidden="true" name="filter_checkbox" onChange="filterTable(this)" type="checkbox"/><span class="passed">128 passed</span>, <input checked="true" class="filter" data-test-result="skipped" disabled="true" hidden="true" name="filter_checkbox" onChange="filterTable(this)" type="checkbox"/><span class="skipped">0 skipped</span>, <input checked="true" class="filter" data-test-result="failed" hidden="true" name="filter_checkbox" onChange="filterTable(this)" type="checkbox"/><span class="failed">1 failed</span>, <input checked="true" class="filter" data-test-result="error" disabled="true" hidden="true" name="filter_checkbox" onChange="filterTable(this)" type="checkbox"/><span class="error">0 errors</span>, <input checked="true" class="filter" data-test-result="xfailed" disabled="true" hidden="true" name="filter_checkbox" onChange="filterTable(this)" type="checkbox"/><span class="xfailed">0 expected failures</span>, <input checked="true" class="filter" data-test-result="xpassed" disabled="true" hidden="true" name="filter_checkbox" onChange="filterTable(this)" type="checkbox"/><span class="xpassed">0 unexpected passes</span>, <input checked="true" class="filter" data-test-result="rerun" disabled="true" hidden="true" name="filter_checkbox" onChange="filterTable(this)" type="checkbox"/><span class="rerun">0 rerun</span>
    <h2>Results</h2>
    <table id="results-table">
      <thead id="results-table-head">
        <tr>
          <th class="sortable result initial-sort" col="result">Result</th>
          <th class="sortable" col="name">Test</th>
          <th class="sortable" col="duration">Duration</th>
          <th class="sortable links" col="links">Links</th></tr>
        <tr hidden="true" id="not-found-message">
          <th colspan="4">No results found. Try to check the filters</th></tr></thead>
      <tbody class="failed results-table-row">
        <tr>
          <td class="col-result">Failed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestContainer::test_confapi_errors</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log">self = &lt;test_config.TestContainer object at 0x7f1e0e04fc40&gt;, container = qutebrowser.config.config.ConfigContainer(config=&lt;qutebrowser.config.config.Config object at 0x7f1e1088d790&gt;, configapi=None, pattern=None, prefix=&#x27;&#x27;)<br/><br/>    def test_confapi_errors(self, container):<br/>&gt;       assert False<br/><span class="error">E       assert False</span><br/><br/>tests/unit/config/test_config.py:778: AssertionError<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_unknown_option[foobar]</td>
          <td class="col-duration">0.07</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_unknown_option[tab]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_unknown_option[tabss]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_unknown_option[tabs.]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_validate[confirm_quit]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_validate[tabs]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_validate[tabs.show]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_call[confirm_quit-confirm_quit-True-True]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_call[confirm_quit-confirm_quit-True-False]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_call[tabs-tabs.show-True-True]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_call[tabs-tabs.show-True-False]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_call[tabs.show-tabs.show-True-True]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_call[tabs.show-tabs.show-True-False]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_call[tabs-None-True-True]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_call[tabs-None-True-False]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_call[tabs-colors.tabs.bar.bg-False-True]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestChangeFilter::test_call[tabs-colors.tabs.bar.bg-False-False]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_validate_invalid_mode</td>
          <td class="col-duration">0.01</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_validate_invalid_type</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_bindings_for_and_get_command[commands0-expected0]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;message-info foo&#x27;, &#x27;b&#x27;: &#x27;message-info bar&#x27;}, &#x27;insert&#x27;: {}, &#x27;hint&#x27;: {}, &#x27;passthrough&#x27;: {}, &#x27;command&#x27;: {}, &#x27;prompt&#x27;: {}, &#x27;caret&#x27;: {}, &#x27;register&#x27;: {}, &#x27;yesno&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: None}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_bindings_for_and_get_command[commands1-expected1]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;message-info foo&#x27;, &#x27;b&#x27;: &#x27;message-info bar&#x27;}, &#x27;insert&#x27;: {}, &#x27;hint&#x27;: {}, &#x27;passthrough&#x27;: {}, &#x27;command&#x27;: {}, &#x27;prompt&#x27;: {}, &#x27;caret&#x27;: {}, &#x27;register&#x27;: {}, &#x27;yesno&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;c&#x27;: &#x27;message-info baz&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_bindings_for_and_get_command[commands2-expected2]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;message-info foo&#x27;, &#x27;b&#x27;: &#x27;message-info bar&#x27;}, &#x27;insert&#x27;: {}, &#x27;hint&#x27;: {}, &#x27;passthrough&#x27;: {}, &#x27;command&#x27;: {}, &#x27;prompt&#x27;: {}, &#x27;caret&#x27;: {}, &#x27;register&#x27;: {}, &#x27;yesno&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;x&#x27;: None}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_bindings_for_empty_command</td>
          <td class="col-duration">0.01</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;,x&#x27;: &#x27;&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_command_unbound</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_command_default</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;x&#x27;: &#x27;message-info default&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;x&#x27;: &#x27;message-info custom&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_reverse_bindings_for[bindings0-expected0]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;open foo&#x27;, &#x27;b&#x27;: &#x27;open bar&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_reverse_bindings_for[bindings1-expected1]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;open foo&#x27;, &#x27;b&#x27;: &#x27;open foo&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_reverse_bindings_for[bindings2-expected2]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;open foo&#x27;, &#x27;&lt;ctrl-a&gt;&#x27;: &#x27;open foo&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_reverse_bindings_for[bindings3-expected3]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;open foo ;; open bar&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_reverse_bindings_for[bindings4-expected4]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;o&#x27;: &#x27;set-cmd-text -s :open&#x27;, &#x27;O&#x27;: &#x27;set-cmd-text -s :open -t&#x27;, &#x27;go&#x27;: &#x27;set-cmd-text :open {url:pretty}&#x27;, &#x27;/&#x27;: &#x27;set-cmd-text /&#x27;, &#x27;?&#x27;: &#x27;set-cmd-text ?&#x27;, &#x27;:&#x27;: &#x27;set-cmd-text :&#x27;, &#x27;a&#x27;: &#x27;set-cmd-text no_leading_colon&#x27;, &#x27;b&#x27;: &#x27;set-cmd-text -s -a :skip_cuz_append&#x27;, &#x27;c&#x27;: &#x27;set-cmd-text --append :skip_cuz_append&#x27;, &#x27;x&#x27;: &#x27;set-cmd-text&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_get_reverse_bindings_for[bindings5-expected5]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;&#x27;, &#x27;b&#x27;: &#x27;notreal&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_bind_duplicate[a]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;Ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
VDEBUG   keyboard:config.py:237 Adding binding a -&gt; message-info foo in mode normal.
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;, &#x27;a&#x27;: &#x27;message-info foo&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_bind_duplicate[&lt;Ctrl-X&gt;]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;Ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
VDEBUG   keyboard:config.py:237 Adding binding &lt;Ctrl+x&gt; -&gt; message-info foo in mode normal.
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;Ctrl+x&gt;&#x27;: &#x27;message-info foo&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_bind_duplicate[b]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;Ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
VDEBUG   keyboard:config.py:237 Adding binding b -&gt; message-info foo in mode normal.
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;message-info foo&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_bind[message-info foo-normal]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}
VDEBUG   keyboard:config.py:237 Adding binding a -&gt; message-info foo in mode normal.
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;message-info foo&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_bind[message-info foo-caret]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}
VDEBUG   keyboard:config.py:237 Adding binding a -&gt; message-info foo in mode caret.
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;message-info foo&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_bind[nop ;; wq-normal]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}
VDEBUG   keyboard:config.py:237 Adding binding a -&gt; nop ;; wq in mode normal.
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop ;; wq&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_bind[nop ;; wq-caret]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}
VDEBUG   keyboard:config.py:237 Adding binding a -&gt; nop ;; wq in mode caret.
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;nop ;; wq&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_bind_mode_changing</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}
VDEBUG   keyboard:config.py:237 Adding binding a -&gt; set-cmd-text :nop ;; rl-beginning-of-line in mode normal.
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;set-cmd-text :nop ;; rl-beginning-of-line&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_bind_default</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;message-info default&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;message-info bound&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_bind_default_unbound</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind[normal-a]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;, &#x27;a&#x27;: None}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind[normal-b]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind[normal-&lt;Ctrl-X&gt;]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;Ctrl+x&gt;&#x27;: None}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind[caret-a]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;, &#x27;a&#x27;: None}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind[caret-b]</td>
          <td class="col-duration">0.04</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind[caret-&lt;Ctrl-X&gt;]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;Ctrl+x&gt;&#x27;: None}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind[prompt-a]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;a&#x27;: None}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind[prompt-b]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;b&#x27;: None}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind[prompt-&lt;Ctrl-X&gt;]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;, &#x27;b&#x27;: &#x27;nop&#x27;, &#x27;&lt;ctrl+x&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;caret&#x27;: {&#x27;b&#x27;: &#x27;nop&#x27;}, &#x27;prompt&#x27;: {&#x27;&lt;Ctrl+x&gt;&#x27;: None}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind_unbound</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbound_twice</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: None}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_unbind_old_syntax</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;&lt;ctrl+q&gt;&#x27;: &#x27;nop&#x27;}}
DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestKeyConfig::test_empty_command</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_init_save_manager</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_value</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: tabs.show = never<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_value_no_backend</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: tabs.show = never<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_unset[True]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: tabs.show = never<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_unset[False]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: tabs.show = never<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_unset_never_set</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_clear[True]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: tabs.show = never
DEBUG    config:config.py:351 Config option changed: content.plugins = True<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_clear[False]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: tabs.show = never
DEBUG    config:config.py:351 Config option changed: content.plugins = True<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_read_yaml</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: content.plugins = True<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_opt_valid</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_no_option_error[&lt;lambda&gt;0]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_no_option_error[&lt;lambda&gt;1]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_no_option_error[&lt;lambda&gt;2]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_no_option_error[&lt;lambda&gt;3]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_no_option_error[&lt;lambda&gt;4]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_no_option_error[&lt;lambda&gt;5]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_no_option_error[&lt;lambda&gt;6]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_no_option_error[&lt;lambda&gt;7]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_no_option_error[&lt;lambda&gt;8]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_for_url</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: content.javascript.enabled = False<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_for_url_fallback[True-True]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_for_url_fallback[False-expected1]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_bindings[value0]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: aliases = {}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_bindings[value1]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: aliases = {}
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;normal&#x27;: {&#x27;a&#x27;: &#x27;nop&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_mutable</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_simple</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[True-True-content.headers.custom]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:552 content.headers.custom was mutated, updating
DEBUG    config:config.py:351 Config option changed: content.headers.custom = {&#x27;X-Answer&#x27;: &#x27;42&#x27;}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[True-True-keyhint.blacklist]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:552 keyhint.blacklist was mutated, updating
DEBUG    config:config.py:351 Config option changed: keyhint.blacklist = [&#x27;foo&#x27;]<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[True-True-bindings.commands]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:552 bindings.commands was mutated, updating
DEBUG    config:config.py:351 Config option changed: bindings.commands = {&#x27;prompt&#x27;: {&#x27;foobar&#x27;: &#x27;nop&#x27;}}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[True-False-content.headers.custom]</td>
          <td class="col-duration">0.04</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[True-False-keyhint.blacklist]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[True-False-bindings.commands]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[False-True-content.headers.custom]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[False-True-keyhint.blacklist]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[False-True-bindings.commands]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[False-False-content.headers.custom]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[False-False-keyhint.blacklist]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_mutable[False-False-bindings.commands]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_mutable_twice</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:552 content.headers.custom was mutated, updating
DEBUG    config:config.py:351 Config option changed: content.headers.custom = {&#x27;X-Foo&#x27;: &#x27;fooval&#x27;, &#x27;X-Bar&#x27;: &#x27;barval&#x27;}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_unknown_mutable</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_copy_non_mutable</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_copy_mutable</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_for_pattern</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: content.javascript.enabled = False<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_obj_for_pattern_no_match</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_str</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_valid[set_obj-True-True]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: content.plugins = True<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_valid[set_obj-True-False]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: content.plugins = True<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_valid[set_str-true-True]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:501 Setting content.plugins (type Bool) to True (converted from &#x27;true&#x27;)
DEBUG    config:config.py:351 Config option changed: content.plugins = True<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_valid[set_str-true-False]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:501 Setting content.plugins (type Bool) to True (converted from &#x27;true&#x27;)
DEBUG    config:config.py:351 Config option changed: content.plugins = True<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_invalid[set_obj]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_invalid[set_str]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_wrong_backend[set_obj]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_wrong_backend[set_str]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:501 Setting hints.find_implementation (type String) to &#x27;javascript&#x27; (converted from &#x27;javascript&#x27;)<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_no_autoconfig_save[set_obj-value0]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_no_autoconfig_save[set_str-{}]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_no_autoconfig_no_save[set_obj-value0]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: bindings.default = {}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_no_autoconfig_no_save[set_str-{}]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:501 Setting bindings.default (type Dict) to {} (converted from &#x27;{}&#x27;)
DEBUG    config:config.py:351 Config option changed: bindings.default = {}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_no_pattern[set_obj]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_set_no_pattern[set_str]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:501 Setting colors.statusbar.normal.bg (type QssColor) to &#x27;#abcdef&#x27; (converted from &#x27;#abcdef&#x27;)<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_dump_userconfig</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: content.plugins = True
DEBUG    config:config.py:351 Config option changed: content.headers.custom = {&#x27;X-Foo&#x27;: &#x27;bar&#x27;}<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_dump_userconfig_default</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_str_benchmark[0]</td>
          <td class="col-duration">1.06</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: content.headers.user_agent = true<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_str_benchmark[1]</td>
          <td class="col-duration">0.27</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: content.headers.user_agent = Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) QtWebEngine/5.7.1 Chrome/49.0.2623.111 Safari/537.36<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_str_benchmark[2]</td>
          <td class="col-duration">0.60</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: content.headers.user_agent = aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestConfig::test_get_dict_benchmark</td>
          <td class="col-duration">0.92</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestContainer::test_getattr_invalid_private</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestContainer::test_getattr_prefix</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestContainer::test_getattr_option[configapi0-rgb]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestContainer::test_getattr_option[None-1]</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestContainer::test_getattr_invalid</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestContainer::test_setattr_option</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="log"> -------------------------------Captured log call-------------------------------- <br/>DEBUG    config:config.py:351 Config option changed: content.cookies.store = False<br/></div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestContainer::test_confapi_missing_prefix</td>
          <td class="col-duration">0.00</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody>
      <tbody class="passed results-table-row">
        <tr>
          <td class="col-result">Passed</td>
          <td class="col-name">tests/unit/config/test_config.py::TestContainer::test_pattern_no_configapi</td>
          <td class="col-duration">0.01</td>
          <td class="col-links"></td></tr>
        <tr>
          <td class="extra" colspan="4">
            <div class="empty log">No log output captured.</div></td></tr></tbody></table></body></html>